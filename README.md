# GitLab GitOps workflow for Kubernetes

This project contain configuration that allows you to register a GitLab Agent for this project. See [.gitlab/agents/c2d-mk8s/config.yaml](./.gitlab/agents/c2d-mk8s/config.yaml). It also contains manifests files for Kubernetes that create two namespaces with a simple application. See [manifests](./manifests/).

This project was created to be used by the project [c2platform/ansible](https://gitlab.com/c2platform/ansible) see [How-to GitLab GitOps workflow for Kubernetes](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes-gitlab-gitops.md).

